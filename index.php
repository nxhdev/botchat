<?php

require_once __DIR__ . DIRECTORY_SEPARATOR . 'vendor/autoload.php';

use juno_okyo\Chatfuel;

$chatfuel = new Chatfuel(TRUE);

//@intelephense-disable-next-line
$client = new \GuzzleHttp\Client();
define("API_KEY", "3NZGJ4SSUU6A3K47MRFE4DHQTMP6OK");


if ($_GET['card'][0] == '5') {
    $url = 'https://shop-procard.com/api/vcards/'.$_GET['card'];
    $response = $client->request('GET', $url);
    // echo $response->getBody(); // '{"id": 1420053, "name": "guzzle", ...}'
    $data = json_decode($response->getBody());
    $raveId = $data->rave_id;

    $url = 'https://strowallet.com/api/card-history';
    $response = $client->request('POST', $url, [
        'form_params' => [
            'public_key' => API_KEY,
            'card_id' => $raveId
        ]
    ]);
    $data = json_decode($response->getBody());

    if (count($data->data) == 0) {
        $chatfuel->sendText('Không có lịch sử giao dịch');
        die();
    }
    foreach ($data->data as $value) {
        $chatfuel->sendText('Giao dịch '.$value->narration.' số tiền '.$value->amount);
    }

} else {
    $url = 'http://157.230.252.65:3000/ccchina1/card/trade?card=' . $_GET['card'] . '&limit=20&page=1';
    $response = $client->request('GET', $url);
    // echo $response->getBody(); // '{"id": 1420053, "name": "guzzle", ...}'
    $data = json_decode($response->getBody());
    $res = '';

    if (count($data->data) == 0) {
        $chatfuel->sendText('Không có lịch sử giao dịch');
        die();
    }
    foreach ($data->data as $value) {
        $chatfuel->sendText('Giao dịch merchant ' . $value->merchant . ' lúc ' . $value->trade_at . ' với số tiền ' . $value->trade_amt);
    }
}
